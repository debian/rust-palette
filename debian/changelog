rust-palette (0.7.4.0+dfsg-3) unstable; urgency=medium

  * reorder autopkgtests to test default features before all
  * add patches cherry-picked upstream
    to avoid recursive trait resolution for IntoIterator

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 23 Feb 2024 09:25:20 +0100

rust-palette (0.7.4.0+dfsg-2) unstable; urgency=medium

  * add patch 2002 to avoid benches

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 17 Feb 2024 05:28:58 +0100

rust-palette (0.7.4.0+dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release
    (publish with added .0
    to override accidentally uploaded not-true-source crate)

  [ Jonas Smedegaard ]
  * drop patches obsoleted by upstream changes;
    stop build- and autopkgtest-dependencies for crate clap
  * update and unfuzz patches
  * bump project versions in virtual packages and autopkgtests
  * provide palette features alloc approx bytemuck libm phf wide;
    fix provide palette-derive (not palette) feature find-crate
  * update copyright info:
    + update source paths
    + update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 17 Feb 2024 02:29:58 +0100

rust-palette (0.7.3+dfsg-3) unstable; urgency=medium

  * add patch cherry-picked upstream
    to prevent dev dependencies from breaking tests and examples,
    superseding patch 1002
  * add patch 1001_clap
    to accept newer branch of crate clap;
    bump build- and autopkgtest-dependencies for crate clap;
    thanks to Peter Green (see bug#1051521)
  * fix autopkgtest-dependency;
    thanks to Peter Green (see bug#1051521)
  * add patch 2001_rand_chacha
    to use crate rand_chacha (not not-in-Debian rand_mt),
    superseding patch 2001_rand_mt;
    build- and autopkgtest-depend on package for crate rand_chacha;
    thanks to Peter Michael Green (see bug#1051521)
  * unfuzz patches; update DEP-3 headers
  * add patch 1004 to add feature guards to tests;
    relax autopkgtests to enable features std approx as needed;
    closes: bug#1051521, thanks to Peter Michael Green

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Dec 2023 12:38:01 +0100

rust-palette (0.7.3+dfsg-2) unstable; urgency=medium

  * update patch 2001_scad to skip excluded regression tests
  * update DEP-3 patch headers
  * drop patch 2001_find-crate,
    obsoleted by Debian package changes;
    provide package for feature find-crate;
    (build-)depend on package for crate find-crate
  * add patch 1003 to install test part of testsuite;
    unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 27 Sep 2023 17:44:14 +0200

rust-palette (0.7.3+dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump project version in virtual packages and autopkgtests
  * drop unneeded patch 2001_clap;
    rename patch 2001_image -> 1001_image;
    adjust build- and autopkgtest-dependencies for crate clap;
    update DEP-3 patch headers
  * add patch 1002 to enable clap feature std
  * add patch 2001_find-crate
    to avoid crate find-crate not in Debian
  * extend patch 2005 to disable autobenches;
    thanks to Peter Green (see bug#1043418)
  * update dh-cargo fork

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 19 Aug 2023 22:43:25 +0200

rust-palette (0.7.2+dfsg-3) unstable; urgency=medium

  * drop patch 2001_phf, obsoleted by Debian package updates;
    tighten (build-)dependency for crate phf;
    closes: bug#1043222, thanks to Andreas Hasenack

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Aug 2023 17:05:43 +0200

rust-palette (0.7.2+dfsg-2) unstable; urgency=medium

  * strip same-workspace path
  * change patch 2005 to avoid bench-only dependencies;
  * renumber all dependency-related patches to 2001_*;
    tighten DEP-3 patch headers
  * add patch 2001_clap
    to relax dependency to match system crate clap v4.1.13
  * update patches 2001_image 2001_ron to relax (not bump) dependencies
    stop build- or autopkgtest-depend on package for crate criterion
  * add patches 2001_enterpolation 2001_rand_mt 2001_scad
    to avoid test-only crates not in Debian
  * check tests during build;
    build-depend on packages for crates
    clap fast-srgb8 image phf rand ron serde serde_json wide
  * fix autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 24 Jul 2023 11:11:56 +0200

rust-palette (0.7.2+dfsg-1) experimental; urgency=medium

  * initial packaging release;
    closes: bug#1040661

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 16 Jul 2023 21:35:10 +0200
