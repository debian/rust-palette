Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Palette
Upstream-Contact: https://github.com/Ogeon/palette/issues
Source: https://github.com/Ogeon/palette
 .
 Repackaged, excluding potentially non-DFSG VCS scripts
Files-Excluded:
 .github/*

Files: *
Copyright:
  2015  Erik Hedvall <hello@erikhedvall.nu>
License-Grant:
 Licensed under either of
  * Apache License, Version 2.0,
    (<LICENSE-APACHE> or <http://www.apache.org/licenses/LICENSE-2.0>)
  * MIT license
    (<LICENSE-MIT> or <http://opensource.org/licenses/MIT>)
 at your option.
License: Expat or Apache-2.0
Reference:
 no_std_test/Cargo.toml
 no_std_test/README.md
 palette/Cargo.toml
 palette/README.md
 palette_derive/Cargo.toml
 LICENSE-APACHE
 LICENSE-MIT

Files:
 palette/integration_tests/hsluv_dataset/hsluv_dataset.json
Copyright:
  2012-2020  Alexei Boronine
  2016       Florian Dormont
License-Grant:
 license = "MIT OR Apache-2.0"
License: Expat or Apache-2.0
Reference:
 palette/integration_tests/hsluv_dataset/LICENSE.hsluv_dataset

Files:
 example-data/input/*
Copyright: None (in the public domain)
License: Public-Domain
 Test images were taken
 from <http://homepages.cae.wisc.edu/~ece533/images/>,
 where they are released
 as "Public-Domain Test Images for Homeworks and Projects".
Reference:
 example-data/input/README.md

Files:
 debian/*
Copyright:
  2023-2024  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This packaging is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3,
 or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

Files:
 debian/debcargo/bin/cargo
Copyright:
  2022       Jonas Smedegaard <dr@jones.dk>
  2015-2016  Luca Bruno <lucab@debian.org>
  2017-2019  Vasudeva Kamath <vasudev@copyninja.info>
  2016-2019  Ximin Luo <infinity0@debian.org>
License: Apache-2.0 or Expat

Files:
 debian/debcargo/lib/*
Copyright:
  2022-2023  Jonas Smedegaard <dr@jones.dk>
  2016       Josh Triplett <josh@joshtriplett.org>
  2018       Ximin Luo <infinity0@debian.org>
License: Expat

License: Apache-2.0
Reference: /usr/share/common-licenses/Apache-2.0

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files
 (the "Software"),
 to deal in the Software without restriction,
 including without limitation
 the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included in all copies or substantial portions
 of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED
 TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3
